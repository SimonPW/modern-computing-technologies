from __future__ import print_function, division

import numpy as np
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
from matplotlib import cm




data = np.loadtxt('fftw3_2d_example.txt',delimiter='\t')

L = 20.0
sigma = 1.0
N = 256
KX = np.fft.ifftshift( data[:,3].reshape((N, N)) )
KY = np.fft.ifftshift( data[:,4].reshape((N, N)) )
F  = np.fft.ifftshift( data[:,5].reshape((N, N)) ) * (L/N)**2

fig = plt.figure(figsize=[20.,16.])
ax = fig.gca(projection='3d')
X, Y, Z = axes3d.get_test_data(0.05)
ax.plot_surface(KX, KY, np.abs(F), rstride=2, cstride=2, alpha=0.75)
cset = ax.contour(KX, KY, np.abs(F), zdir='z', offset= 0.0, cmap=cm.coolwarm)
cset = ax.contour(KX, KY, np.abs(F), zdir='x', offset= KY.min(), cmap=cm.coolwarm)
cset = ax.contour(KX, KY, np.abs(F), zdir='y', offset=-KX.min(), cmap=cm.coolwarm)

#ax.scatter( KX, KY, np.exp( -(KX**2+KY**2)/(2.0*sigma) ) )
#ax.plot_surface(KX, KY, 2.0*np.pi* np.exp( -(KX**2+KY**2)/(2.0*sigma) ), rstride=5, cstride=5, alpha=1.0)

ax.set_xlabel(r'$k_x$',fontsize=16)
ax.set_xlim(KX.min(),-KX.min())
ax.set_ylabel(r'$k_y$',fontsize=16)
ax.set_ylim(KY.min(),-KY.min())
ax.set_zlabel(r'$\mathcal{F}[f](k_x,k_y)$',fontsize=16)
#ax.set_zlim(-100, 100)

print('test kx:     ',KX.min(),np.pi*N/L)
print('test max val:',np.abs(F).max(),2.0*np.pi )

#plt.show()
plt.savefig('fftw3_2d_example.png')
plt.close(fig)



plt.grid(True)
plt.scatter( np.sqrt(KX**2+KY**2).flatten(), 2.0*np.pi * np.exp( -(KX**2+KY**2)/(2.0*sigma) ).flatten(), s=0.5 )
plt.scatter( np.sqrt(KX**2+KY**2).flatten(),np.abs(F).flatten(), s=0.5 )
plt.show()