from __future__ import print_function, division

import numpy as np
import scipy.sparse.csgraph as csgraph



graphA = np.loadtxt('graphA.txt')
graphB = np.loadtxt('graphB.txt')

print(csgraph.csgraph_from_dense(graphA))


C = graphA.dot(graphB)


print()
print('A')
print(graphA)
print('B')
print(graphB)

print()

graphC = np.loadtxt('graphC.txt')

print('C')
print(graphC)
print(C)
print(graphC-C)


#print('trace:    ',np.diag(graphC).sum(),  '/',np.diag(C).sum())
#print('triangles:',np.diag(graphC).sum()/6,'/',np.diag(C).sum()/6)