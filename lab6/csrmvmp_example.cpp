#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <cuda_runtime.h>
#include <cublas_v2.h>
#include <cusparse.h>

/*
 * How to compile (assume cuda is installed at /usr/local/cuda/)
 *   nvcc -std=c++11 -o csrmvmp_example.x csrmvmp_example.cpp -Xcompiler -fopenmp -I/usr/local/cuda/include  -L/usr/local/cuda/lib64 -lcublas -lcusparse -lcudart
 *
 */
void printMatrix(int m, int n, const double*A, int lda, const char* name)
{
    for(int row = 0 ; row < m ; row++){
        for(int col = 0 ; col < n ; col++){
            double Areg = A[row + col*lda];
            printf("%s(%d,%d) = %f\n", name, row+1, col+1, Areg);
        }
    }
}

int main(int argc, char*argv[])
{
    cublasHandle_t cublasH = NULL;
    cusparseHandle_t cusparseH = NULL;
    cudaStream_t stream = NULL;
    cusparseMatDescr_t descrA = NULL;

    cublasStatus_t cublasStat = CUBLAS_STATUS_SUCCESS;
    cusparseStatus_t cusparseStat = CUSPARSE_STATUS_SUCCESS;
    cudaError_t cudaStat1 = cudaSuccess;
    cudaError_t cudaStat2 = cudaSuccess;
    cudaError_t cudaStat3 = cudaSuccess;
    cudaError_t cudaStat4 = cudaSuccess;
    cudaError_t cudaStat5 = cudaSuccess;
    const int n = 4;
    const int nnzA = 9;
/* 
 *      |    1     0     2     3   |
 *      |    0     4     0     0   |
 *  A = |    5     0     6     7   |
 *      |    0     8     0     9   |
 *
 * eigevales are { -0.5311, 7.5311, 9.0000, 4.0000 }
 *
 * The largest eigenvaluse is 9 and corresponding eigenvector is
 *
 *      | 0.3029  |
 * v =  |     0   |
 *      | 0.9350  |
 *      | 0.1844  |
 */


// =========================== prepare matrix A =====================================

 
    const int csrRowPtrA[n+1] = { 0, 3, 4, 7, 9 };
    const int csrColIndA[nnzA] = {0, 2, 3, 1, 0, 2, 3, 1, 3 };
    const double csrValA[nnzA] = {1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0 };
    const double lambda_exact[n] = { 9.0000, 7.5311, 4.0000, -0.5311 };
    const double x0[n] = {1.0, 2.0, 3.0, 4.0 }; /* initial guess */
    double x[n]; /* numerical eigenvector */

    int *d_csrRowPtrA = NULL;
    int *d_csrColIndA = NULL;
    double *d_csrValA = NULL;

    double *d_x = NULL; /* eigenvector */
    double *d_y = NULL; /* workspace */

    const double tol = 1.e-6;
    const int max_ites = 1000;

    const double h_one  = 1.0;
    const double h_zero = 0.0;

    printf("example of csrmv_mp \n");
    printf("tol = %E \n", tol);
    printf("max. iterations = %d \n", max_ites);

    printf("1st eigenvaluse is %f\n", lambda_exact[0] );
    printf("2nd eigenvaluse is %f\n", lambda_exact[1] );

    double alpha = lambda_exact[1]/lambda_exact[0] ;
    printf("convergence rate is %f\n", alpha );

    double est_iterations = log(tol)/log(alpha);
    printf("# of iterations required is %d\n", (int)ceil(est_iterations)  );

/* step 1: create cublas/cusparse handle, bind a stream */
    cudaStat1 = cudaStreamCreateWithFlags(&stream, cudaStreamNonBlocking);
    assert(cudaSuccess == cudaStat1);

    cublasStat = cublasCreate(&cublasH);
    assert(CUBLAS_STATUS_SUCCESS == cublasStat);

    cublasStat = cublasSetStream(cublasH, stream);
    assert(CUBLAS_STATUS_SUCCESS == cublasStat);

    cusparseStat = cusparseCreate(&cusparseH);
    assert(CUSPARSE_STATUS_SUCCESS == cusparseStat);

    cusparseStat = cusparseSetStream(cusparseH, stream);
    assert(CUSPARSE_STATUS_SUCCESS == cusparseStat);

/* step 2: configuration of matrix A */
    cusparseStat = cusparseCreateMatDescr(&descrA);
    assert(CUSPARSE_STATUS_SUCCESS == cusparseStat);

    cusparseSetMatIndexBase(descrA,CUSPARSE_INDEX_BASE_ZERO);
    cusparseSetMatType(descrA, CUSPARSE_MATRIX_TYPE_GENERAL );


// ========================================= power method ================================================

 
/* step 3: copy A and x0 to device */
    cudaStat1 = cudaMalloc ((void**)&d_csrRowPtrA, sizeof(int) * (n+1) );
    cudaStat2 = cudaMalloc ((void**)&d_csrColIndA, sizeof(int) * nnzA );
    cudaStat3 = cudaMalloc ((void**)&d_csrValA   , sizeof(double) * nnzA );
    cudaStat4 = cudaMalloc ((void**)&d_x         , sizeof(double) * n );
    cudaStat5 = cudaMalloc ((void**)&d_y         , sizeof(double) * n );
    assert(cudaSuccess == cudaStat1);
    assert(cudaSuccess == cudaStat2);
    assert(cudaSuccess == cudaStat3);
    assert(cudaSuccess == cudaStat4);
    assert(cudaSuccess == cudaStat5);

    cudaStat1 = cudaMemcpy(d_csrRowPtrA, csrRowPtrA, sizeof(int) * (n+1)   , cudaMemcpyHostToDevice);
    cudaStat2 = cudaMemcpy(d_csrColIndA, csrColIndA, sizeof(int) * nnzA    , cudaMemcpyHostToDevice);
    cudaStat3 = cudaMemcpy(d_csrValA   , csrValA   , sizeof(double) * nnzA , cudaMemcpyHostToDevice);
    assert(cudaSuccess == cudaStat1);
    assert(cudaSuccess == cudaStat2);
    assert(cudaSuccess == cudaStat3);


/*
 * step 4: power method
 */
    double lambda = 0.0;
    double lambda_next = 0.0;

/*
 *  4.1: initial guess x0
 */
    cudaStat1 = cudaMemcpy(d_x, x0, sizeof(double) * n, cudaMemcpyHostToDevice);
    assert(cudaSuccess == cudaStat1);

    for(int ite = 0 ; ite < max_ites ; ite++ ){
/*
 *  4.2: normalize vector x
 *      x = x / |x|
 */
        double nrm2_x;
        cublasStat = cublasDnrm2_v2(cublasH,
                                    n,
                                    d_x,
                                    1, // incx,
                                    &nrm2_x  /* host pointer */
                                   );
        assert(CUBLAS_STATUS_SUCCESS == cublasStat);

        double one_over_nrm2_x = 1.0 / nrm2_x;
        cublasStat = cublasDscal_v2( cublasH,
                                     n,
                                     &one_over_nrm2_x,  /* host pointer */
                                     d_x,
                                     1 // incx
                                    );
        assert(CUBLAS_STATUS_SUCCESS == cublasStat);


  // matrix-vector multiplication (sparse matrix)
/*
 *  4.3: y = A*x
 */
        cusparseStat = cusparseDcsrmv_mp(cusparseH,
                                         CUSPARSE_OPERATION_NON_TRANSPOSE,
                                         n,
                                         n,
                                         nnzA,
                                         &h_one,
                                         descrA,
                                         d_csrValA,
                                         d_csrRowPtrA,
                                         d_csrColIndA,
                                         d_x,
                                         &h_zero,
                                         d_y);
        assert(CUSPARSE_STATUS_SUCCESS == cusparseStat);

/*
 *  4.4: lambda = y**T*x
 */
        cublasStat = cublasDdot_v2 ( cublasH,
                                     n,
                                     d_x,
                                     1, // incx,
                                     d_y,
                                     1, // incy,
                                     &lambda_next  /* host pointer */
                                   );
        assert(CUBLAS_STATUS_SUCCESS == cublasStat);

        double lambda_err = fabs( lambda_next - lambda_exact[0] );
        printf("ite %d: lambda = %f, error = %E\n", ite, lambda_next, lambda_err );
/*
 *  4.5: check if converges
 */
        if ( (ite > 0) &&
             fabs( lambda - lambda_next ) < tol
        ){
            break; // converges
        }

/*
 *  4.6: x := y
 *       lambda = lambda_next
 *
 *  so new approximation is (lambda, x), x is not normalized.
 */
        cudaStat1 = cudaMemcpy(d_x, d_y, sizeof(double) * n , cudaMemcpyDeviceToDevice);
        assert(cudaSuccess == cudaStat1);

        lambda = lambda_next;
    }


// ============================================ report largest eigenvalue and eigenvector ==========================================

 
/*
 * step 5: report eigen-pair
 */
    cudaStat1 = cudaMemcpy(x, d_x, sizeof(double) * n, cudaMemcpyDeviceToHost);
    assert(cudaSuccess == cudaStat1);

    printf("largest eigenvalue is %E\n", lambda );

    printf("eigenvector = (matlab base-1)\n");
    printMatrix(n, 1, x, n, "V0");
    printf("=====\n");


/* free resources */
    if (d_csrRowPtrA  ) cudaFree(d_csrRowPtrA);
    if (d_csrColIndA  ) cudaFree(d_csrColIndA);
    if (d_csrValA     ) cudaFree(d_csrValA);
    if (d_x           ) cudaFree(d_x);
    if (d_y           ) cudaFree(d_y);

    if (cublasH       ) cublasDestroy(cublasH);
    if (cusparseH     ) cusparseDestroy(cusparseH);
    if (stream        ) cudaStreamDestroy(stream);
    if (descrA        ) cusparseDestroyMatDescr(descrA);

    cudaDeviceReset();

    return 0;
}


