==4702== NVPROF is profiling process 4702, command: ./expected_val.exe
==4702== Profiling application: ./expected_val.exe
==4702== Profiling result:
"Time(%)","Time","Calls","Avg","Min","Max","Name"
%,ms,,ms,ms,ms,
25.007323,46.953241,100,0.469532,0.464853,0.485364,"void kernel_RC_mult<int=256, int=80, int=80, int=1>(double const *, double2 const *, double2*)"
20.080126,37.702035,100,0.377020,0.372663,0.383927,"void kernel_DZdreduce_threadfence<double, unsigned int=128, bool=0>(double const *, double2*, double*, unsigned int)"
19.483198,36.581255,100,0.365812,0.360856,0.376535,"void dot_kernel<double2, double2, double2, int=64, int=1, int=1>(cublasDotParams<double2, double2>)"
17.521744,32.898469,100,0.328984,0.318873,0.347224,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::transform_iterator<thrust::detail::zipped_binary_op<int, evaluate_functional>, thrust::zip_iterator<thrust::tuple<thrust::device_ptr<double>, thrust::device_ptr<double2>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>, int, thrust::use_default>, thrust::system::cuda::detail::aligned_decomposition<long>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, int, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
14.646135,27.499285,100,0.274992,0.273017,0.286041,"void kernel_DZdreduce<unsigned int=512>(double*, double2*, double*, int)"
1.692506,3.177814,3,1.059271,0.001088,2.115119,"[CUDA memcpy HtoD]"
0.459598,0.862931,100,0.008629,0.008480,0.009567,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
0.299883,0.563055,400,0.001407,0.001312,0.001696,"[CUDA memcpy DtoH]"
0.277407,0.520853,102,0.005106,0.003168,0.195164,"[CUDA memset]"
0.219768,0.412631,100,0.004126,0.004032,0.004256,"void reduce_1Block_kernel<double2, double2, double2, int=64, int=6>(double2*, int, double2*)"
0.175731,0.329949,100,0.003299,0.003200,0.003552,"void __reduce_kernel__<unsigned int=512, double>(double*, double*, int)"
0.136583,0.256445,100,0.002564,0.002528,0.002656,"void __reduce_kernel__<unsigned int=64, double>(double*, double*, int)"

==4702== API calls:
No API activities were profiled.
