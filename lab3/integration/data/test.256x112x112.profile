==4847== NVPROF is profiling process 4847, command: ./expected_val.exe
==4847== Profiling application: ./expected_val.exe
==4847== Profiling result:
"Time(%)","Time","Calls","Avg","Min","Max","Name"
%,ms,,ms,ms,ms,
25.539879,92.522777,100,0.925227,0.916555,0.943594,"void kernel_RC_mult<int=256, int=112, int=112, int=1>(double const *, double2 const *, double2*)"
20.279241,73.465175,100,0.734651,0.727759,0.746574,"void kernel_DZdreduce_threadfence<double, unsigned int=128, bool=0>(double const *, double2*, double*, unsigned int)"
19.818579,71.796347,100,0.717963,0.709712,0.733583,"void dot_kernel<double2, double2, double2, int=64, int=1, int=1>(cublasDotParams<double2, double2>)"
17.096142,61.933832,100,0.619338,0.607442,0.635697,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::transform_iterator<thrust::detail::zipped_binary_op<int, evaluate_functional>, thrust::zip_iterator<thrust::tuple<thrust::device_ptr<double>, thrust::device_ptr<double2>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>, int, thrust::use_default>, thrust::system::cuda::detail::aligned_decomposition<long>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, int, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
14.693042,53.228172,100,0.532281,0.529236,0.545172,"void kernel_DZdreduce<unsigned int=512>(double*, double2*, double*, int)"
1.699909,6.158225,3,2.052741,0.000960,4.101601,"[CUDA memcpy HtoD]"
0.245550,0.889549,100,0.008895,0.008672,0.009536,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
0.187516,0.679311,102,0.006659,0.003168,0.353208,"[CUDA memset]"
0.158527,0.574294,400,0.001435,0.001344,0.006400,"[CUDA memcpy DtoH]"
0.116014,0.420280,100,0.004202,0.003967,0.004960,"void reduce_1Block_kernel<double2, double2, double2, int=64, int=6>(double2*, int, double2*)"
0.094275,0.341527,100,0.003415,0.003296,0.003616,"void __reduce_kernel__<unsigned int=512, double>(double*, double*, int)"
0.071326,0.258390,100,0.002583,0.002528,0.002688,"void __reduce_kernel__<unsigned int=64, double>(double*, double*, int)"

==4847== API calls:
No API activities were profiled.
