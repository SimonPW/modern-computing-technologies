==5090== NVPROF is profiling process 5090, command: ./expected_val.exe
==5090== Profiling application: ./expected_val.exe
==5090== Profiling result:
"Time(%)","Time","Calls","Avg","Min","Max","Name"
%,ms,,ms,ms,ms,
25.793508,189.448986,100,1.894489,1.878452,2.063824,"void kernel_RC_mult<int=256, int=160, int=160, int=1>(double const *, double2 const *, double2*)"
20.451148,150.210251,100,1.502102,1.482910,1.772407,"void kernel_DZdreduce_threadfence<double, unsigned int=128, bool=0>(double const *, double2*, double*, unsigned int)"
19.847410,145.775893,100,1.457758,1.446271,1.483357,"void dot_kernel<double2, double2, double2, int=64, int=1, int=1>(cublasDotParams<double2, double2>)"
16.903169,124.150942,100,1.241509,1.221444,1.261219,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::transform_iterator<thrust::detail::zipped_binary_op<int, evaluate_functional>, thrust::zip_iterator<thrust::tuple<thrust::device_ptr<double>, thrust::device_ptr<double2>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>, int, thrust::use_default>, thrust::system::cuda::detail::aligned_decomposition<long>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, int, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
14.818598,108.840112,100,1.088401,1.074919,1.366689,"void kernel_DZdreduce<unsigned int=512>(double*, double2*, double*, int)"
1.706260,12.532191,3,4.177397,0.000960,8.321248,"[CUDA memcpy HtoD]"
0.148102,1.087785,102,0.010664,0.003424,0.731823,"[CUDA memset]"
0.115252,0.846504,100,0.008465,0.008224,0.009408,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
0.078181,0.574226,400,0.001435,0.001312,0.008287,"[CUDA memcpy DtoH]"
0.058497,0.429648,100,0.004296,0.003872,0.005055,"void reduce_1Block_kernel<double2, double2, double2, int=64, int=6>(double2*, int, double2*)"
0.047714,0.350453,100,0.003504,0.003360,0.003936,"void __reduce_kernel__<unsigned int=512, double>(double*, double*, int)"
0.032162,0.236223,100,0.002362,0.002304,0.002464,"void __reduce_kernel__<unsigned int=64, double>(double*, double*, int)"

==5090== API calls:
No API activities were profiled.
