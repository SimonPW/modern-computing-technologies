==5161== NVPROF is profiling process 5161, command: ./expected_val.exe
==5161== Profiling application: ./expected_val.exe
==5161== Profiling result:
"Time(%)","Time","Calls","Avg","Min","Max","Name"
%,ms,,ms,ms,ms,
25.997935,231.478358,100,2.314783,2.275436,2.835391,"void kernel_RC_mult<int=256, int=176, int=176, int=1>(double const *, double2 const *, double2*)"
20.584521,183.278830,100,1.832788,1.792407,2.104239,"void kernel_DZdreduce_threadfence<double, unsigned int=128, bool=0>(double const *, double2*, double*, unsigned int)"
19.801760,176.309346,100,1.763093,1.746871,1.797142,"void dot_kernel<double2, double2, double2, int=64, int=1, int=1>(cublasDotParams<double2, double2>)"
16.754652,149.178745,100,1.491787,1.468191,1.516510,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::transform_iterator<thrust::detail::zipped_binary_op<int, evaluate_functional>, thrust::zip_iterator<thrust::tuple<thrust::device_ptr<double>, thrust::device_ptr<double2>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>, int, thrust::use_default>, thrust::system::cuda::detail::aligned_decomposition<long>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, int, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
14.729463,131.147025,100,1.311470,1.299842,1.570748,"void kernel_DZdreduce<unsigned int=512>(double*, double2*, double*, int)"
1.723317,15.343934,3,5.114644,0.000960,10.229236,"[CUDA memcpy HtoD]"
0.134182,1.194723,102,0.011712,0.003168,0.868204,"[CUDA memset]"
0.095264,0.848208,100,0.008482,0.008288,0.009280,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
0.063838,0.568400,400,0.001421,0.001343,0.001728,"[CUDA memcpy DtoH]"
0.050110,0.446163,100,0.004461,0.004063,0.005152,"void reduce_1Block_kernel<double2, double2, double2, int=64, int=6>(double2*, int, double2*)"
0.036942,0.328921,100,0.003289,0.003136,0.003616,"void __reduce_kernel__<unsigned int=512, double>(double*, double*, int)"
0.028015,0.249437,100,0.002494,0.002432,0.002720,"void __reduce_kernel__<unsigned int=64, double>(double*, double*, int)"

==5161== API calls:
No API activities were profiled.
