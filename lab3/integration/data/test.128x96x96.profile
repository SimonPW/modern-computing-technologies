==4335== NVPROF is profiling process 4335, command: ./expected_val.exe
==4335== Profiling application: ./expected_val.exe
==4335== Profiling result:
"Time(%)","Time","Calls","Avg","Min","Max","Name"
%,ms,,ms,ms,ms,
24.674388,33.725764,100,0.337257,0.333784,0.356344,"void kernel_RC_mult<int=128, int=96, int=96, int=1>(double const *, double2 const *, double2*)"
20.008552,27.348346,100,0.273483,0.269210,0.282138,"void kernel_DZdreduce_threadfence<double, unsigned int=128, bool=0>(double const *, double2*, double*, unsigned int)"
19.307029,26.389482,100,0.263894,0.260730,0.273657,"void dot_kernel<double2, double2, double2, int=64, int=1, int=1>(cublasDotParams<double2, double2>)"
17.675874,24.159966,100,0.241599,0.233882,0.252698,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::transform_iterator<thrust::detail::zipped_binary_op<int, evaluate_functional>, thrust::zip_iterator<thrust::tuple<thrust::device_ptr<double>, thrust::device_ptr<double2>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>, int, thrust::use_default>, thrust::system::cuda::detail::aligned_decomposition<long>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, int, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
14.565651,19.908810,100,0.199088,0.197500,0.210971,"void kernel_DZdreduce<unsigned int=512>(double*, double2*, double*, int)"
1.664399,2.274955,3,0.758318,0.001088,1.520988,"[CUDA memcpy HtoD]"
0.613376,0.838383,100,0.008383,0.008159,0.009248,"void thrust::system::cuda::detail::bulk_::detail::launch_by_value<unsigned int=128, thrust::system::cuda::detail::bulk_::detail::cuda_task<thrust::system::cuda::detail::bulk_::parallel_group<thrust::system::cuda::detail::bulk_::concurrent_group<thrust::system::cuda::detail::bulk_::agent<unsigned long=7>, unsigned long=128>, unsigned long=0>, thrust::system::cuda::detail::bulk_::detail::closure<thrust::system::cuda::detail::reduce_detail::reduce_partitions, thrust::tuple<thrust::system::cuda::detail::bulk_::detail::cursor<unsigned int=1>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::detail::normal_iterator<thrust::pointer<int, thrust::system::cuda::detail::tag, thrust::use_default, thrust::use_default>>, thrust::plus<double>, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type, thrust::null_type>>>>(unsigned long=7)"
0.410280,0.560784,400,0.001401,0.001312,0.001760,"[CUDA memcpy DtoH]"
0.366476,0.500912,102,0.004910,0.003392,0.146300,"[CUDA memset]"
0.305447,0.417495,100,0.004174,0.004064,0.004384,"void reduce_1Block_kernel<double2, double2, double2, int=64, int=6>(double2*, int, double2*)"
0.246429,0.336827,100,0.003368,0.003199,0.003520,"void __reduce_kernel__<unsigned int=512, double>(double*, double*, int)"
0.162099,0.221562,100,0.002215,0.002175,0.002400,"void __reduce_kernel__<unsigned int=64, double>(double*, double*, int)"

==4335== API calls:
No API activities were profiled.
